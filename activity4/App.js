import Home from './screens/Home';
import Introduction from './screens/Introduction';
import Explanation from './screens/Explanation';
import Thoughts from './screens/Thoughts';
import Conclusion from './screens/Conclusion';



import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

export default function App(){
  return (
        <NavigationContainer>


          <Stack.Navigator>
            <Stack.Screen name ="Home" component={Home}/>
            <Stack.Screen name ="Introduction" component={Introduction}/>
            <Stack.Screen name ="Explanation" component={Explanation}/>
            <Stack.Screen name ="Thoughts" component={Thoughts}/>
            <Stack.Screen name ="Conclusion" component={Conclusion}/>
          </Stack.Navigator>
        </NavigationContainer>

  );

 
}