import React from 'react'
import {View, Text ,Button,StyleSheet, ImageBackground} from 'react-native'
import { useNavigation } from '@react-navigation/native'
import screen4 from '../bg/screen4.png'

const Thoughts = () =>{
    const navigation = useNavigation();
    return(
        <ImageBackground source={screen4}
        style={styles.imgbackground}> 
            
            <View style ={styles.TextPosition}>
            <Text style ={styles.Text1}>Thoughts:</Text>
            </View>

            <View style ={styles.TextPosition1}>
            <Text style ={styles.Text2}>My thoughts about this course was that the coding subjects would be always sitting in front of a computer when in reality there are some subjects that are cultured or language based. </Text>
            </View>

            <View style ={styles.ButtonPosition}>
            <Button 
            color='#6C63F6'
            title ="continue"
            onPress={() =>navigation.navigate('Conclusion')}>
            </Button>
            </View>

        </ImageBackground>
    )
}
    const styles = StyleSheet.create({
            imgbackground:{
                flex:1,
                alignItems:'center', 
                justifyContent:'center',
                height:'105%',
                width:'100%',
                },
            ButtonPosition:{
                    position:'absolute',
                    bottom: 50,
                },
            TextPosition:{
                    flex:1,
                    alignContent:'center',
                    justifyContent:'center',
                    position:'absolute',
                    bottom:600,
                },
            Text1:{
                    color:'#0B0C0C',
                    fontSize:30,
                    fontWeight: 'bold',
                },
            TextPosition1:{
                    flex:1,
                    alignContent:'center',
                    position:'absolute',
                    justifyContent:'center',
                    bottom: 100,
                    width:300,
                },
            Text2:{
                    color:'#515FEA',
                    fontSize:18,
                    fontStyle:'italic',
                    textAlign:'center',
                    fontWeight:'700',
                },
            });

export default Thoughts