import React from 'react'
import {View, Text ,Button, ImageBackground, StyleSheet} from 'react-native'
import { useNavigation } from '@react-navigation/native'
import screen5 from '../bg/screen5.png'

const Conclusion = () =>{
    const navigation = useNavigation();
    return(
        <ImageBackground source={screen5}
        style={styles.imgbackground}> 

            <View style ={styles.TextPosition}>
            <Text style ={styles.Text1}>Conclusion: </Text>
            </View>

            <View style ={styles.TextPosition1}>
            <Text style ={styles.Text2}>In my conclusion, I think we'll see more computer based activities when we are on a higher year level and it will be more harder and more pressure. </Text>
            </View>

            <View style ={styles.ButtonPosition}>
            <Button 
            color='#6C63F6'
            title ="Home"
            onPress={() =>navigation.navigate('Home')}>
            </Button>
            </View>
        </ImageBackground>
    )
}
const styles = StyleSheet.create({
    imgbackground:{
        flex:1,
        alignItems:'center', 
        justifyContent:'center',
        height:'110%',
        width:'100%',
        position:'absolute',
        bottom:10,
        left:1,
        },
    ButtonPosition:{
            position:'absolute',
            bottom: 400,
        },
    TextPosition:{
            flex:1,
            alignContent:'center',
            justifyContent:'center',
            position:'absolute',
            bottom:550,
        },
    Text1:{
            color:'#030200',
            fontSize:20,
            fontWeight: 'bold',
        },
    TextPosition1:{
            flex:1,
            alignContent:'center',
            position:'absolute',
            justifyContent:'center',
            bottom: 450,
            width:350,
        },
    Text2:{
            color:'#F6758B',
            fontSize:18,
            textAlign:'center',
            fontWeight:'700',
        },
    });

export default Conclusion