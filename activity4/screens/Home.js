import React from 'react'
import {View, Text ,Button, ImageBackground, StyleSheet} from 'react-native'
import { useNavigation } from '@react-navigation/native'
import screen1 from '../bg/screen1.png'


const Home = () =>{
    const navigation = useNavigation();
    return(
        <ImageBackground source={screen1} resizeMode='cover'
        style={styles.imgbackground}>

        <View style ={styles.TextPosition}>
        <Text style ={styles.Text1}>Information Technology</Text>
        </View>

        <View style ={styles.TextPosition1}>
        <Text style ={styles.Text2}>Why did I choose the course </Text>
        </View>
        
        <View style ={styles.TextPosition2}>
        <Text style ={styles.Text3}>By: Kenneth C. De Paz</Text>
        </View>

        <View style ={styles.ButtonPosition}>
        <Button
            color='#6C63F6'
            title='continue'
            onPress={() =>navigation.navigate('Introduction')}>
        </Button>
        </View>
        </ImageBackground>
        
    )

}

    const styles = StyleSheet.create({
        imgbackground: {
            flex:1,
            alignItems:'center', 
            justifyContent:'center',        
        },
        ButtonPosition:{
            position:'absolute',
            bottom: 100,
        },
        TextPosition:{
            flex:1,
            alignContent:'center',
            justifyContent:'center',
            position:'absolute',
            bottom:330,
        },
        Text1:{
            color:'#2908F7',
            fontSize:35,
            fontWeight: 'bold',
        },
        TextPosition1:{
            flex:1,
            alignContent:'center',
            position:'absolute',
            justifyContent:'center',
            bottom: 500,
        },
        Text2:{
            color:'#000000',
            fontSize:20,
        },
        TextPosition2:{
            flex:1,
            alignContent:'center',
            position:'absolute',
            justifyContent:'center',
            bottom: 150,
        },
        Text3:{
            color:'#000000',
            fontSize:18,
        },


    });

export default Home;