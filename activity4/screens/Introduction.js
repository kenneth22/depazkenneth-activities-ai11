import React from 'react'
import {View, Text ,Button,StyleSheet, ImageBackground} from 'react-native'
import { useNavigation } from '@react-navigation/native'
import screen2 from '../bg/screen2.png'

const Reasons = () =>{
    const navigation = useNavigation();
    return(
        <ImageBackground source={screen2} resizeMode='stretch'
        style={styles.imgbackground}>

            <View style ={styles.TextPosition}>
        <   Text style ={styles.Text1}>Introduction:</Text>
            </View>

            <View style ={styles.TextPosition1}>
            <Text style ={styles.Text2}>I choose Information Technology as my course because of my curiousity in computers and how they work. </Text>
            </View>
            
            <View style ={styles.TextPosition2}>
            <Text style ={styles.Text3}>And for the big salary. </Text>
            </View>
            
            <View style ={styles.ButtonPosition}>
            <Button 
            color='#6C63F6'
            title ="continue"
            onPress={() =>navigation.navigate('Explanation')}>
            </Button>
            </View>

        </ImageBackground>
    )
}

const styles = StyleSheet.create({
        imgbackground:{
            flex:1,
            alignItems:'center', 
            justifyContent:'center',        
        },
    
        ButtonPosition:{
            position:'absolute',
            bottom: 100,
        },
        TextPosition:{
            flex:1,
            alignContent:'center',
            justifyContent:'center',
            position:'absolute',
            bottom:280,
        },
    Text1:{
            color:'#060606',
            fontSize:25,
            fontWeight: 'bold',
        },
    TextPosition1:{
            flex:1,
            alignContent:'center',
            position:'absolute',
            justifyContent:'center',
            bottom: 200,
            left:120,
            width:230,
        },
    Text2:{
            color:'#020005',
            fontSize:15,
            textAlign:'center',
        },
    TextPosition2:{
            flex:1,
            alignContent:'center',
            position:'absolute',
            justifyContent:'center',
            bottom: 150,
            left:130,
            width:230
        },
    Text3:{
            color:'#020005',
            fontSize:20,
            
        },
})

export default Reasons