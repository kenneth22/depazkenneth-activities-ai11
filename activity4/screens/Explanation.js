import screen3 from '../bg/screen3.png'
import React from 'react'
import {View, Text ,Button,StyleSheet, ImageBackground} from 'react-native'
import { useNavigation } from '@react-navigation/native'

const Expectations = () =>{
    const navigation = useNavigation();
    return(
        <ImageBackground source={screen3}
        style={styles.imgbackground}>

            <View style ={styles.TextPosition}>
            <Text style ={styles.Text1}>Explanation:</Text>
            </View>

            <View style ={styles.TextPosition1}>
            <Text style ={styles.Text2}>I choose the BSIT path because it provided more "hands on" experience than Computer Science, which is more theoretical. As I progressed through the program, I learned that the deadline of this activities makes the programmers feel pressure. </Text>
            </View>

            <View style ={styles.ButtonPosition}>
            <Button 
            color='#6C63F6'
            title ="continue"
            onPress={() =>navigation.navigate('Thoughts')}>
            </Button>
            </View>
        </ImageBackground>
        
    )
}
    const styles = StyleSheet.create({
        imgbackground: {
            flex:1,
            alignItems:'center', 
            justifyContent:'center', 
            width:360,
            height:650,       
        },
        ButtonPosition:{
            position:'absolute',
            bottom: 50,
        },
        TextPosition:{
            flex:1,
            alignContent:'center',
            justifyContent:'center',
            position:'absolute',
            bottom:600,
        },
        Text1:{
            color:'#030200',
            fontSize:30,
            fontWeight: 'bold',
        },
        TextPosition1:{
            flex:1,
            alignContent:'center',
            position:'absolute',
            justifyContent:'center',
            bottom: 100,
            width:350,
        },
        Text2:{
            color:'#515FEA',
            fontSize:17,
            fontStyle:'italic',
            textAlign:'center',
            fontWeight:'700',
        },
    })

export default Expectations;