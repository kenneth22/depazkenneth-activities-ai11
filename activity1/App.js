import { useState }  from 'react';
import {StyleSheet, Button , View, Alert, TextInput} from 'react-native';

export default function App() {
const [input, setInput] = useState('');
{
  return (
    <View style={styles.container}>
      <TextInput 
        style={styles.input}  
        onChangeText ={(value) => setInput(value)}
        />
    
       <Button
        title="Will alert if pressed"
        onPress={() => alert ("You just typed: "+ input)}
       />
       
    </View>
  );
  }
}
const styles = StyleSheet.create({
  input: {
    width: 200,
    borderColor: "#999",
    borderWidth: 3,
    borderRadius: 10,
    padding: 5,
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});