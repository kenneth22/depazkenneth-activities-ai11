import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button,ImageBackground } from 'react-native';
import Home from './screens/Home';
import Grams from './screens/Grams';
import Kilograms from './screens/Kilograms';
import Pounds from './screens/Pounds';
import Ton from './screens/Ton';
import Decagrams from './screens/Decagrams';
import Hectograms from './screens/Hectograms';
import Decigrams from './screens/Decigrams';
import Centigrams from './screens/Centigrams';
import Milligrams from './screens/Milligrams';


import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();

export default function App(){
  return (
        <NavigationContainer>
          <Stack.Navigator>
            <Stack.Screen name ="Home" component={Home} options={{headerShown: false}}/>
            <Stack.Screen name ="Grams" component={Grams} options={{headerShown: true}}/>
            <Stack.Screen name ="Kilograms" component={Kilograms} options={{headerShown: true}}/>
            <Stack.Screen name ="Pounds" component={Pounds} options={{headerShown: true}}/>
            <Stack.Screen name ="Ton" component={Ton} options={{headerShown: true}}/>
            <Stack.Screen name ="Decagrams" component={Decagrams} options={{headerShown: true}}/>
            <Stack.Screen name ="Hectograms" component={Hectograms} options={{headerShown: true}}/>
            <Stack.Screen name ="Decigrams" component={Decigrams} options={{headerShown: true}}/>
            <Stack.Screen name ="Centigrams" component={Centigrams} options={{headerShown: true}}/>
            <Stack.Screen name ="Milligrams" component={Milligrams} options={{headerShown: true}}/>
          </Stack.Navigator>
        </NavigationContainer>

  );

 
}