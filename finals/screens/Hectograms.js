import React, { Component } from 'react';
import { Text, View, ImageBackground, StyleSheet, TextInput, TouchableOpacity, Image, Button} from 'react-native'

import { useNavigation } from '@react-navigation/native'

export default class Grams extends Component {

  componentDidMount() {
  }
  state = {
    height: 0,
      weight: 0,
        ResultToKiloGrams: 0,
            ResultToDecaGrams: 0,
              ResultToGrams: 0,
                ResultToPounds: 0,
                  ResultToTon: 0,
                    ResultToDeciGrams: 0,
                     ResultToCentiGrams: 0,
                      ResultToMilliGrams: 0,

  }

  HandleCalculationKiloGrams = () => {
    let value = (this.state.weight) * 0.1;
    this.setState({
      ResultToKiloGrams: value,
    });
  }

  HandleCalculationGrams = () => {
    let value = (this.state.weight) * 100;
    this.setState({
      ResultToGrams: value,
    });
  }

  HandleCalculationDecaGrams = () => {
    let value = (this.state.weight) * 10;
    this.setState({
      ResultToDecaGrams: value,
    });
  }

  HandleCalculationPounds = () => {
    let value = (this.state.weight) * 0.220462;
    this.setState({
      ResultToPounds: value,
    });
  }

  HandleCalculationTon = () => {
    let value = (this.state.weight) * 0.000110231;
    this.setState({
      ResultToTon: value,
    });
  }

  HandleCalculationDeciGrams = () => {
    let value = (this.state.weight) * 1000;
    this.setState({
      ResultToDeciGrams: value,
    });
  }

  HandleCalculationCentiGrams = () => {
    let value = (this.state.weight) * 10000;
    this.setState({
      ResultToCentiGrams: value,
    });
  }

  HandleCalculationMilliGrams = () => {
    let value = (this.state.weight) * 100000;
    this.setState({
      ResultToMilliGrams: value.toFixed(),
    });
  }

  render() {
    return (

        <View style={styles.container}>

          <View style={styles.intro}>
            <TextInput 
             placeholder="Weight(Hectograms)" keyboardType="numeric" style={styles.input} onChangeText={weight => this.setState({ weight })} />
          </View>

          <TouchableOpacity onPress={this.HandleCalculationKiloGrams}>
            <Text style={styles.buttonTextKiloGrams}>Convert to Kilograms</Text>
          </TouchableOpacity>
          <Text style={styles.resultKiloGrams}>{this.state.ResultToKiloGrams} Kg</Text>

          <TouchableOpacity onPress={this.HandleCalculationGrams}>
            <Text style={styles.buttonTextGrams}>Convert to Grams</Text>
          </TouchableOpacity>
          <Text style={styles.resultGrams}>{this.state.ResultToGrams} Grams</Text>

          <TouchableOpacity onPress={this.HandleCalculationPounds}>
            <Text style={styles.buttonTextPounds}>Convert to Pounds</Text>
          </TouchableOpacity>
          <Text style={styles.resultPounds}>{this.state.ResultToPounds} Tons</Text>

          <TouchableOpacity onPress={this.HandleCalculationDecaGrams}>
          <Text style={styles.buttonTextDecaGrams}>Convert to Decagram</Text>
          </TouchableOpacity>
          <Text style={styles.resultDeca}>{this.state.ResultToDecaGrams} Dag </Text>

          <TouchableOpacity onPress={this.HandleCalculationTon}>
            <Text style={styles.buttonTextTon}>Convert to Ton</Text>
          </TouchableOpacity>
          <Text style={styles.resultTon}>{this.state.ResultToTon} t </Text>

          <TouchableOpacity onPress={this.HandleCalculationDeciGrams}>
            <Text style={styles.buttonTextDeciGrams}>Convert to Decigram</Text>
          </TouchableOpacity>
          <Text style={styles.resultDeci}>{this.state.ResultToDeciGrams} Dg </Text>

          <TouchableOpacity onPress={this.HandleCalculationCentiGrams}>
            <Text style={styles.buttonTextCentiGrams}>Convert to Centigram</Text>
          </TouchableOpacity>
          <Text style={styles.resultCenti}>{this.state.ResultToCentiGrams} Cg</Text>

          <TouchableOpacity onPress={this.HandleCalculationMilliGrams}>
            <Text style={styles.buttonTextMilliGrams}>Convert to Milligram</Text>
          </TouchableOpacity> 
          <Text style={styles.resultMilli}>{this.state.ResultToMilliGrams} Mg </Text>
          
        </View >
    );
  }
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    height: '100%',
    width: '100%',
   
  },

  back: {
    marginTop: 20,
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
    alignContent:'center',
    borderRadius: 30,
    backgroundColor: 'red',
    width: '30%',
    textAlign: 'center',
    alignSelf: 'center',
  },

  container: {
    flex: 1,
  },

  intro: {
    alignItems: 'center',
  },

  input: {
    alignItems: 'center',
    width: '70%',
    fontSize: 30,
    marginTop: 20,
    color: 'black',
    backgroundColor: 'white',
    textAlign: 'center',
    borderRadius: 30,
  },

  buttonTextKiloGrams: {
    marginTop: 20,
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
    alignContent:'center',
    borderRadius: 30,
    backgroundColor: 'black',
    width: '60%',
    textAlign: 'center',
    alignSelf: 'center',
  },

  buttonTextGrams: {
    marginTop: 20,
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
    alignContent:'center',
    borderRadius: 30,
    backgroundColor: 'black',
    width: '60%',
    textAlign: 'center',
    alignSelf: 'center',
  },

  buttonTextDecaGrams: {
    marginTop: 20,
    fontSize: 19,
    color: 'white',
    fontWeight: 'bold',
    alignContent:'center',
    borderRadius: 30,
    backgroundColor: 'black',
    width: '60%',
    textAlign: 'center',
    alignSelf: 'center',
  },

  buttonTextPounds: {
    marginTop: 20,
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
    alignContent:'center',
    borderRadius: 30,
    backgroundColor: 'black',
    width: '60%',
    textAlign: 'center',
    alignSelf: 'center',
  },

  buttonTextTon: {
    marginTop: 20,
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
    alignContent:'center',
    borderRadius: 30,
    backgroundColor: 'black',
    width: '60%',
    textAlign: 'center',
    alignSelf: 'center',
  },

  buttonTextDeciGrams: {
    marginTop: 20,
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
    alignContent:'center',
    borderRadius: 30,
    backgroundColor: 'black',
    width: '60%',
    textAlign: 'center',
    alignSelf: 'center',
  },

  buttonTextCentiGrams: {
    marginTop: 20,
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
    alignContent:'center',
    borderRadius: 30,
    backgroundColor: 'black',
    width: '60%',
    textAlign: 'center',
    alignSelf: 'center',
  },

  buttonTextMilliGrams: {
    marginTop: 20,
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
    alignContent:'center',
    borderRadius: 30,
    backgroundColor: 'black',
    width: '60%',
    textAlign: 'center',
    alignSelf: 'center',
  },

  resultKiloGrams: {
    width: '90%',
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: 'white',
    borderRadius: 30,
    alignSelf: 'center'
  },

  resultDeca: {
    width: '90%',
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: 'white',
    borderRadius: 30,
    alignSelf: 'center'
  },

  resultTon: {
    width: '90%',
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: 'white',
    borderRadius: 30,
    alignSelf: 'center'
  },

  resultCenti: {
    width: '90%',
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: 'white',
    borderRadius: 30,
    alignSelf: 'center'
  },

  resultGrams: {
    width: '90%',
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: 'white',
    borderRadius: 30,
    alignSelf: 'center'
  },

  resultPounds: {
    width: '90%',
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: 'white',
    borderRadius: 30,
    alignSelf: 'center'
  },

  resultDeci: {
    width: '90%',
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: 'white',
    borderRadius: 30,
    alignSelf: 'center'
  },

  resultMilli: {
    width: '90%',
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: 'white',
    borderRadius: 30,
    alignSelf: 'center'
  },

});