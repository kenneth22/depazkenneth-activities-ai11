import React, { Component } from 'react';
import { Text, View, ImageBackground, StyleSheet, TextInput, TouchableOpacity, Image, Button} from 'react-native'

import { useNavigation } from '@react-navigation/native'

export default class Grams extends Component {

  componentDidMount() {
  }
  state = {
    height: 0,
      weight: 0,
        ResultToKiloGrams: 0,
            ResultToDecaGrams: 0,
              ResultToGrams: 0,
                ResultToTonne: 0,
                  ResultToHectoGrams: 0,
                    ResultToDeciGrams: 0,
                     ResultToCentiGrams: 0,
                      ResultToMilliGrams: 0,

  }

  HandleCalculationKiloGrams = () => {
    let value = (this.state.weight) * 0.453592;
    this.setState({
      ResultToKiloGrams: value,
    });
  }

  HandleCalculationGrams = () => {
    let value = (this.state.weight) * 453.592;
    this.setState({
      ResultToGrams: value,
    });
  }

  HandleCalculationDecaGrams = () => {
    let value = (this.state.weight) * 45.3592;
    this.setState({
      ResultToDecaGrams: value,
    });
  }

  HandleCalculationTonne = () => {
    let value = (this.state.weight) * 0.0005;
    this.setState({
      ResultToTonne: value,
    });
  }

  HandleCalculationHectoGrams = () => {
    let value = (this.state.weight) * 4.53592;
    this.setState({
      ResultToHectoGrams: value,
    });
  }

  HandleCalculationDeciGrams = () => {
    let value = (this.state.weight) * 4535.92;
    this.setState({
      ResultToDeciGrams: value,
    });
  }

  HandleCalculationCentiGrams = () => {
    let value = (this.state.weight) * 45359.2;
    this.setState({
      ResultToCentiGrams: value,
    });
  }

  HandleCalculationMilliGrams = () => {
    let value = (this.state.weight) * 453592;
    this.setState({
      ResultToMilliGrams: value.toFixed(),
    });
  }

  render() {
    return (

        <View style={styles.container}>

          <View style={styles.intro}>
            <TextInput 
             placeholder="Weight(Pounds)" keyboardType="numeric" style={styles.input} onChangeText={weight => this.setState({ weight })} />
          </View>

          <TouchableOpacity onPress={this.HandleCalculationKiloGrams}>
            <Text style={styles.buttonTextKiloGrams}>Convert to Kilograms</Text>
          </TouchableOpacity>
          <Text style={styles.resultKiloGrams}>{this.state.ResultToKiloGrams} Kg</Text>

          <TouchableOpacity onPress={this.HandleCalculationGrams}>
            <Text style={styles.buttonTextGrams}>Convert to Grams</Text>
          </TouchableOpacity>
          <Text style={styles.resultGrams}>{this.state.ResultToGrams} Grams</Text>

          <TouchableOpacity onPress={this.HandleCalculationTonne}>
            <Text style={styles.buttonTextTon}>Convert to Ton</Text>
          </TouchableOpacity>
          <Text style={styles.resultTon}>{this.state.ResultToTonne} Tons</Text>

          <TouchableOpacity onPress={this.HandleCalculationDecaGrams}>
          <Text style={styles.buttonTextDecaGrams}>Convert to Decagram</Text>
          </TouchableOpacity>
          <Text style={styles.resultDeca}>{this.state.ResultToDecaGrams} Dag </Text>

          <TouchableOpacity onPress={this.HandleCalculationHectoGrams}>
            <Text style={styles.buttonTextHectoGrams}>Convert to Hectogram</Text>
          </TouchableOpacity>
          <Text style={styles.resultHecto}>{this.state.ResultToHectoGrams} Hg </Text>

          <TouchableOpacity onPress={this.HandleCalculationDeciGrams}>
            <Text style={styles.buttonTextDeciGrams}>Convert to Decigram</Text>
          </TouchableOpacity>
          <Text style={styles.resultDeci}>{this.state.ResultToDeciGrams} Dg </Text>

          <TouchableOpacity onPress={this.HandleCalculationCentiGrams}>
            <Text style={styles.buttonTextCentiGrams}>Convert to Centigram</Text>
          </TouchableOpacity>
          <Text style={styles.resultCenti}>{this.state.ResultToCentiGrams} Cg</Text>

          <TouchableOpacity onPress={this.HandleCalculationMilliGrams}>
            <Text style={styles.buttonTextMilliGrams}>Convert to Milligram</Text>
          </TouchableOpacity> 
          <Text style={styles.resultMilli}>{this.state.ResultToMilliGrams} Mg </Text>
          
        </View >
    );
  }
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    height: '100%',
    width: '100%',
   
  },

  back: {
    marginTop: 20,
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
    alignContent:'center',
    borderRadius: 30,
    backgroundColor: 'red',
    width: '30%',
    textAlign: 'center',
    alignSelf: 'center',
  },

  container: {
    flex: 1,
  },

  intro: {
    alignItems: 'center',
  },

  input: {
    alignItems: 'center',
    width: '70%',
    fontSize: 30,
    marginTop: 20,
    color: 'black',
    backgroundColor: 'white',
    textAlign: 'center',
    borderRadius: 30,
  },

  buttonTextKiloGrams: {
    marginTop: 20,
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
    alignContent:'center',
    borderRadius: 30,
    backgroundColor: 'black',
    width: '60%',
    textAlign: 'center',
    alignSelf: 'center',
  },

  buttonTextGrams: {
    marginTop: 20,
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
    alignContent:'center',
    borderRadius: 30,
    backgroundColor: 'black',
    width: '60%',
    textAlign: 'center',
    alignSelf: 'center',
  },

  buttonTextDecaGrams: {
    marginTop: 20,
    fontSize: 19,
    color: 'white',
    fontWeight: 'bold',
    alignContent:'center',
    borderRadius: 30,
    backgroundColor: 'black',
    width: '60%',
    textAlign: 'center',
    alignSelf: 'center',
  },

  buttonTextTon: {
    marginTop: 20,
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
    alignContent:'center',
    borderRadius: 30,
    backgroundColor: 'black',
    width: '60%',
    textAlign: 'center',
    alignSelf: 'center',
  },

  buttonTextHectoGrams: {
    marginTop: 20,
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
    alignContent:'center',
    borderRadius: 30,
    backgroundColor: 'black',
    width: '60%',
    textAlign: 'center',
    alignSelf: 'center',
  },

  buttonTextDeciGrams: {
    marginTop: 20,
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
    alignContent:'center',
    borderRadius: 30,
    backgroundColor: 'black',
    width: '60%',
    textAlign: 'center',
    alignSelf: 'center',
  },

  buttonTextCentiGrams: {
    marginTop: 20,
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
    alignContent:'center',
    borderRadius: 30,
    backgroundColor: 'black',
    width: '60%',
    textAlign: 'center',
    alignSelf: 'center',
  },

  buttonTextMilliGrams: {
    marginTop: 20,
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
    alignContent:'center',
    borderRadius: 30,
    backgroundColor: 'black',
    width: '60%',
    textAlign: 'center',
    alignSelf: 'center',
  },

  resultKiloGrams: {
    width: '90%',
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: 'white',
    borderRadius: 30,
    alignSelf: 'center'
  },

  resultDeca: {
    width: '90%',
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: 'white',
    borderRadius: 30,
    alignSelf: 'center'
  },

  resultHecto: {
    width: '90%',
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: 'white',
    borderRadius: 30,
    alignSelf: 'center'
  },

  resultCenti: {
    width: '90%',
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: 'white',
    borderRadius: 30,
    alignSelf: 'center'
  },

  resultGrams: {
    width: '90%',
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: 'white',
    borderRadius: 30,
    alignSelf: 'center'
  },

  resultTon: {
    width: '90%',
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: 'white',
    borderRadius: 30,
    alignSelf: 'center'
  },

  resultDeci: {
    width: '90%',
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: 'white',
    borderRadius: 30,
    alignSelf: 'center'
  },

  resultMilli: {
    width: '90%',
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: 'white',
    borderRadius: 30,
    alignSelf: 'center'
  },

});