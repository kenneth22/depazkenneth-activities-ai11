import React from 'react'
import {View, Text ,Button, ImageBackground, TouchableOpacity, StyleSheet} from 'react-native'
import { useNavigation } from '@react-navigation/native'



const Home = () =>{
    const navigation = useNavigation();
    return( 
        
        <View
        style={{flex: 1,width: '100%', alignContent: 'center', alignItems: 'center',}}>   
        <Text style={{fontSize: 40, fontWeight: 'bold', color: 'black', marginTop: 20, marginBottom: 20, textDecorationLine: 'underline',}}>Weight Converter</Text>
        <Text style={{fontSize: 30, fontWeight: 'bold', color: 'black'}}>Choose Your Weight Unit</Text>
        <TouchableOpacity onPress={() =>navigation.navigate('Grams')}>
            <Text style={styles.Button}> Gram</Text>
          </TouchableOpacity> 

          <TouchableOpacity onPress={() =>navigation.navigate('Kilograms')}>
            <Text style={styles.Button}> Kilogram</Text>
          </TouchableOpacity> 

          <TouchableOpacity onPress={() =>navigation.navigate('Pounds')}>
            <Text style={styles.Button}> Pounds</Text>
          </TouchableOpacity> 

          <TouchableOpacity onPress={() =>navigation.navigate('Ton')}>
            <Text style={styles.Button}>  Ton  </Text>
          </TouchableOpacity> 

          <TouchableOpacity onPress={() =>navigation.navigate('Decagrams')}>
            <Text style={styles.Button}>  Decagram  </Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={() =>navigation.navigate('Hectograms')}>
            <Text style={styles.Button}>  Hectogram  </Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={() =>navigation.navigate('Decigrams')}>
            <Text style={styles.Button}>  Decigram  </Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={() =>navigation.navigate('Centigrams')}>
            <Text style={styles.Button}> Centigram</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={() =>navigation.navigate('Milligrams')}>
            <Text style={styles.Button}> Milligram</Text>
          </TouchableOpacity>

            </View>
    )

}

const styles = StyleSheet.create({

    Button:{
        fontSize: 30,
        fontWeight: 'bold',
        backgroundColor: 'black',
        color: 'white',
        borderRadius: 30,
        textAlign: 'center',
        marginTop: 20,
        
    },
})

export default Home